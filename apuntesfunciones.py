d = {
    'LHY': 'Roberto',
    'HRT': 'Juan',
    'EEr': 'Maria',
}

print(d)


def print_message(name):
    print(f'Hello world! My name is {name}')


def sum_list(start: int, end: int) -> int:
    new_list = list(range(start, end + 1))
    s = sum(new_list)
    return s


a = sum_list(1, 3)
print(a)


def sum_product(*args):
    x = args[0] + 2 * sum(args[1:])
    return x


from math import sin, cos


def trig(x: float) -> float:
    y = sin(x) ** 2 + cos(x) ** 2
    return y


print(trig(2))

print(sorted([2,3,2], reverse=true))



def f(student):
    return student[2]



def sum_product(my_list: list) -> int:
    x = my_list[0] + 2 * sum(my_list[1:])
    return x


print(sum_product([3, 3, 4, 5]))

def square(n):
    return n * n


count = 0
print('Starting')
Starting
while count < 10:
    print(count)
    count += 1
print('done')




print('Print out values in a range')
for i in range(0, 10):
    print(i, ' ', end='')
print()
print('Done')



words = ['yesir', 'we', 'ji']
for w in words:
    print(w, len(w))


print('Only print code if all iterations completed')
num = int(input('Enter a number to check fro: '))
for i in range(0, 6):
    if i == num:
        break
    print(i, ' ', end='')


for i in range(0, 10):
    print(i, '', end='')
    if i % 2 == 1:
        continue
    print('hey its an even number')
    print('we love even numbers')
print('done')

