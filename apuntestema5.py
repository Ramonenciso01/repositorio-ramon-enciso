# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
S = [3,2,4,5]
A = S
A
[3, 2, 4, 5]
length(S)
Traceback (most recent call last):
  File "<input>", line 1, in <module>
NameError: name 'length' is not defined
len(S)
4
T = (3,2,4,5)
len(T)
4
S[0]
3
type(S)
<class 'list'>
type(T)
<class 'tuple'>
S[0:2]
[3, 2]
r = S[0:2]
len(r)
2
type(r)
<class 'list'>
S = [:2]
  File "<input>", line 1
    S = [:2]
         ^
SyntaxError: invalid syntax
S[:2]
[3, 2]
S[:len(S)]
[3, 2, 4, 5]
S[0:4:2]
[3, 4]

T[0]=1
Traceback (most recent call last):
  File "<input>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment

# El de arriba da error porque un tupla es inmutable, eso solo se podría hacer con una lista

S[0]=1
S
[1, 2, 4, 5]

# Conjuntos:

c1 = {3,4}
c2 = {3,5,7}

# Para hacer una unión

c1.union(c2)
{3, 4, 5, 7}






