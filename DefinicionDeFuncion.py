# Tarea definir funcion
# Ramón Enciso Ortega
# 2ºA Business Analytics


def funcion(my_list: list) -> int:
    x = my_list[0] + 2*sum(my_list[1:])
    return x

print(funcion([2,3,5]))
